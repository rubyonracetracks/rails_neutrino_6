[![pipeline status](https://gitlab.com/rubyonracetracks/rails_neutrino_7/badges/main/pipeline.svg)](https://gitlab.com/rubyonracetracks/rails_neutrino_7/-/commits/main) 

# Rails Neutrino 7: Automatic Rails App Generator

Welcome to Rails Neutrino! This repository automatically creates a new high-quality Rails 6 app from scratch. It takes just a few minutes to complete a task that would require many hours of grueling work to do manually.

